# Information

Based on a flutter template project. Pages, BeerData class and api service were created from empty files.

# Setup
```
fvm use
fvm flutter pub get
cp .env.sample .env
./setup_configs.sh
./update_l10n_files.sh
```

# Comment

The design is minimal, there was no time to improve it.