import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/config.dart';
import 'package:beer_tinder/src/application/init_app.dart';
import 'package:beer_tinder/src/application/app.dart';
import 'package:feedback/feedback.dart';

Future<void> main() async {
  await initApp();
  runApp(
    const ProviderScope(
      child: BetterFeedback(child: App()),
    ),
  );
}
