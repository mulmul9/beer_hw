import 'dart:ui';

import 'config_values.dart';

class Config {
  static String get apiUrl {
    String api;

    if (configs['apiUrl'] != null) {
      api = configs['apiUrl'].toString();
    } else {
      api = 'http://localhost:3002';
    }

    return api;
  }

  static bool get isLocalDev {
    return configs['environment'] != null &&
        configs['environment'] == 'localDev';
  }

  static String get environment {
    if (configs['environment'] == null && !isLocalDev) {
      throw Exception('No environment found');
    }
    return configs['environment'] ?? '';
  }

  static String get buildNumber {
    return configs["buildNumber"] ?? '1500';
  }

  static Locale get fallbackLocale => const Locale('en');

  static String? get sentryDSN {
    return configs['sentryDSN'] ?? '';
  }

  static String get appVersion {
    if (configs['appVersion'] == null && !isLocalDev) {
      throw Exception('Not found brands api version');
    }

    return configs['appVersion'] ?? '';
  }
}
