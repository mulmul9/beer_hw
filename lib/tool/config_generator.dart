import 'dart:convert';
import 'dart:io';
import 'package:dotenv/dotenv.dart';

Future<void> main() async {
  var env = DotEnv(includePlatformEnvironment: true)..load();

  final config = {
    'environment': env['ENVIRONMENT'],
    'sentryDSN': env['SENTRY_DSN'],
    'buildNumber': Platform.environment['GITHUB_RUN_NUMBER'],
    'buildTime': DateTime.now().toUtc().toString(),
  };

  const filename = 'lib/config_values.dart';
  File(filename).writeAsString('final configs = ${json.encode(config)};');
}
