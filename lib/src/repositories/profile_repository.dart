import 'dart:async';

import 'package:beer_tinder/src/widgets/beer_data.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/src/services/logger.dart';


class ProfileRepository extends ChangeNotifier {
  final Ref ref;
  ProfileRepository(this.ref);

  List<BeerData> selectedBeers = [];

}
