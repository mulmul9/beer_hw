import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/src/repositories/profile_repository.dart';

class Store {
  static final ChangeNotifierProvider<ProfileRepository> profile =
      ChangeNotifierProvider(ProfileRepository.new);
}
