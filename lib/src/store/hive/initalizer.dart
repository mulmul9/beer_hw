import 'package:hive_flutter/hive_flutter.dart';
import 'package:beer_tinder/src/services/locale_service.dart';

Future<void> initHiveBoxes() async {
  await Hive.initFlutter();
  await Future.wait([
    LocaleService.initBox(),
  ]);
}
