import 'package:beer_tinder/src/application/app_router.gr.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/src/util/loading_states.dart';
import 'package:beer_tinder/src/services/services.dart';

class LikedBeersViewModel extends StateNotifier<LoadingStates> {
  final Ref ref;

  LikedBeersViewModel(LoadingStates state, this.ref) : super(state);

  navigateToDetails(int index) {
    ref.read(Services.navigator).router.navigate(DetailsRoute(index: index));
  }
}
