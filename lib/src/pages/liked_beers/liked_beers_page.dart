import 'package:beer_tinder/src/theme/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/src/layouts/main_layout.dart';
import 'package:beer_tinder/src/pages/liked_beers/liked_beers_view_model.dart';
import 'package:beer_tinder/src/store/store.dart';
import 'package:beer_tinder/src/util/loading_states.dart';
import 'package:beer_tinder/src/theme/styles.dart';

class LikedBeersPage extends StatefulWidget {
  const LikedBeersPage({Key? key}) : super(key: key);

  @override
  State<LikedBeersPage> createState() => _LikedBeersPageState();
}

class _LikedBeersPageState extends State<LikedBeersPage> {
  final viewProvider =
      AutoDisposeStateNotifierProvider<LikedBeersViewModel, LoadingStates>(
    (ref) => LikedBeersViewModel(LoadingStates.empty, ref),
  );

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      children: Consumer(
        builder: (context, ref, child) {
          final viewModel = ref.read(viewProvider.notifier);
          final state = ref.watch(viewProvider);
          final beersList = ref.read(Store.profile).selectedBeers;

          return SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(
                top: UiKitConstants.kVerticalSpace24,
                left: UiKitConstants.kHorizontalSpace8,
              ),
              child: ListView.builder(
                itemCount: beersList.length,
                itemBuilder: (context, index) {
                  return ListTile(
                    minVerticalPadding: UiKitConstants.kVerticalSpace24,
                    leading: SizedBox(
                      width: 60.0, // Adjust the width as needed
                      height: 80.0, // Adjust the height as needed
                      child: beersList[index].imageUrl != ''
                          ? Image.network(
                              //scale: 0.3,
                              beersList[index].imageUrl,
                              fit: BoxFit.fitHeight,
                            )
                          : const Center(
                              child: Text(
                                'No img',
                                style: TextStyle(color: Colors.black),
                              ),
                            ),
                    ),
                    title: Text(
                      beersList[index].name,
                      style: TextStyle(color: $styles.colors.primary),
                    ),
                    onTap: () {
                      viewModel.navigateToDetails(index);
                    },
                  );
                },
              ),
            ),
          );
        },
      ),
    );
  }
}
