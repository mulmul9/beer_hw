import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/generated/l10n.dart';
import 'package:beer_tinder/src/layouts/main_layout.dart';
import 'package:beer_tinder/src/pages/details/details_view_model.dart';
import 'package:beer_tinder/src/store/store.dart';
import 'package:beer_tinder/src/util/loading_states.dart';
import 'package:beer_tinder/src/theme/app_theme.dart';

class DetailsPage extends StatefulWidget {
  final int index;

  const DetailsPage({Key? key, required this.index}) : super(key: key);

  @override
  State<DetailsPage> createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {
  @override
  void initState() {
    super.initState();
  }

  final viewProvider =
      AutoDisposeStateNotifierProvider<DetailsViewModel, LoadingStates>(
    (ref) => DetailsViewModel(LoadingStates.empty, ref),
  );

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      children: Consumer(
        builder: (context, ref, child) {
          final viewModel = ref.read(viewProvider.notifier);
          final state = ref.watch(viewProvider);
          final user = ref.watch(Store.profile);
          final beer = ref.read(Store.profile).selectedBeers[widget.index];

          return SingleChildScrollView(
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                        top: UiKitConstants.kHorizontalSpace24,
                        bottom: UiKitConstants.kHorizontalSpace24,
                      ),
                      child: SizedBox(
                        height: 250,
                        child: Image.network(alignment: Alignment.center,
                          beer.imageUrl,
                          loadingBuilder: (
                            BuildContext context,
                            Widget child,
                            ImageChunkEvent? loadingProgress,
                          ) {
                            if (loadingProgress == null) {
                              return child;
                            } else {
                              return const Center(
                                child: CircularProgressIndicator(),
                              );
                            }
                          },
                          errorBuilder: (
                            BuildContext context,
                            Object error,
                            StackTrace? stackTrace,
                          ) {
                            return Text(S.of(context).error_loadingImage);
                          },
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        beer.name,
                        style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 24.0),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        beer.description ?? 'No description',
                        style: const TextStyle(color: Colors.black),
                        textAlign: TextAlign.center,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
