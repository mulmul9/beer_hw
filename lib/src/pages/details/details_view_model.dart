import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/src/util/loading_states.dart';

class DetailsViewModel extends StateNotifier<LoadingStates> {
  final Ref ref;

  DetailsViewModel(LoadingStates state, this.ref) : super(state);
}
