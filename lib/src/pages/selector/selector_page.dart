import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/generated/l10n.dart';
import 'package:beer_tinder/src/layouts/main_layout.dart';
import 'package:beer_tinder/src/pages/selector/selector_view_model.dart';
import 'package:beer_tinder/src/theme/app_theme.dart';
import 'package:beer_tinder/src/theme/styles.dart';
import 'package:beer_tinder/src/util/loading_states.dart';

class SelectorPage extends StatefulWidget {
  const SelectorPage({Key? key}) : super(key: key);

  @override
  State<SelectorPage> createState() => _SelectorPageState();
}

class _SelectorPageState extends State<SelectorPage> {
  final viewProvider =
      AutoDisposeStateNotifierProvider<SelectorViewModel, LoadingStates>(
    (ref) => SelectorViewModel(LoadingStates.empty, ref),
  );

  @override
  Widget build(BuildContext context) {
    return MainLayout(
      children: Consumer(
        builder: (context, ref, child) {
          final viewModel = ref.read(viewProvider.notifier);
          final state = ref.watch(viewProvider);

          return Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(
                  top: UiKitConstants.kVerticalSpace80,
                  left: UiKitConstants.kHorizontalSpace24,
                  right: UiKitConstants.kHorizontalSpace16,
                ),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    S.of(context).selector_title,
                    style: $styles.text.quote1.copyWith(
                      color: $styles.colors.primary,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(
                  top: UiKitConstants.kHorizontalSpace24,
                  bottom: UiKitConstants.kHorizontalSpace24,
                ),
                child: SizedBox(
                  height: 250,
                  child: Image.network(
                    viewModel.randomBeer.imageUrl,
                    loadingBuilder: (
                      BuildContext context,
                      Widget child,
                      ImageChunkEvent? loadingProgress,
                    ) {
                      if (loadingProgress == null) {
                        return child;
                      } else {
                        return const Center(
                          child: CircularProgressIndicator(),
                        );
                      }
                    },
                    errorBuilder: (
                      BuildContext context,
                      Object error,
                      StackTrace? stackTrace,
                    ) {
                      return Text(S.of(context).error_loadingImage);
                    },
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  viewModel.randomBeer.name,
                  style: $styles.text.h3.copyWith(
                    color: $styles.colors.primary,
                  ),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.all(UiKitConstants.kHorizontalSpace16),
                child: Text(
                  viewModel.randomBeer.tagline,
                  style: TextStyle(color: $styles.colors.primary),
                ),
              ),
              Padding(
                padding:
                    const EdgeInsets.only(top: UiKitConstants.kVerticalSpace16),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    FloatingActionButton(
                      onPressed: viewModel.dislikeBeer,
                      backgroundColor: Colors.red,
                      heroTag: 'dislike',
                      child: const Icon(Icons.close),
                    ),
                    FloatingActionButton(
                      onPressed: viewModel.likeBeer,
                      backgroundColor: Colors.green,
                      heroTag: 'like',
                      child: const Icon(Icons.check),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }
}
