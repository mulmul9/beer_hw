import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/src/application/app_router.gr.dart';
import 'package:beer_tinder/src/services/services.dart';
import 'package:beer_tinder/src/store/store.dart';
import 'package:beer_tinder/src/util/loading_states.dart';
import 'package:beer_tinder/src/widgets/beer_data.dart';

class SelectorViewModel extends StateNotifier<LoadingStates> {
  final Ref ref;

  BeerData randomBeer = const BeerData(
    name: 'unknown',
    id: -1,
    tagline: 'unknown',
    imageUrl: '',
  );
  int beerCounter = 0;

  SelectorViewModel(LoadingStates state, this.ref) : super(state) {
    init();
  }

  init() async {
    state = LoadingStates.update;
    randomBeer = await ref.read(Services.api).fetchRandomBeer();
    beerCounter = 1;
    state = LoadingStates.done;
  }

  likeBeer() async {
    state = LoadingStates.update;
    if(beerCounter < 10) {
      ref.read(Store.profile).selectedBeers.add(randomBeer);
      randomBeer = await ref.read(Services.api).fetchRandomBeer();
      beerCounter += 1;
    } else {
      ref.read(Services.navigator).router.navigate(const LikedBeersRoute());
    }
    state = LoadingStates.done;
  }

  dislikeBeer() async {
    state = LoadingStates.update;
    if(beerCounter < 10) {
      randomBeer = await ref.read(Services.api).fetchRandomBeer();
      beerCounter += 1;
    } else {
      ref.read(Services.navigator).router.navigate(const LikedBeersRoute());
    }
    state = LoadingStates.done;
  }
}
