import 'package:auto_route/auto_route.dart';
import 'package:beer_tinder/src/pages/details/details_page.dart';
import 'package:beer_tinder/src/pages/selector/selector_page.dart';
import 'package:beer_tinder/src/pages/liked_beers/liked_beers_page.dart';

@CustomAutoRouter(
  transitionsBuilder: TransitionsBuilders.slideLeftWithFade,
  durationInMilliseconds: 300,
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute<void>>[
    AutoRoute(
      path: '/selector',
      page: SelectorPage,
      initial: true,
    ),
    AutoRoute(
      path: '/liked-beers',
      page: LikedBeersPage,
    ),
    AutoRoute(
      path: '/details',
      page: DetailsPage,
    ),
  ],
)
// extend the generated private router
class $AppRouter {}
