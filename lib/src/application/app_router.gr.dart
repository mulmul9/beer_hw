// **************************************************************************
// AutoRouteGenerator
// **************************************************************************

// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// AutoRouteGenerator
// **************************************************************************
//
// ignore_for_file: type=lint

// ignore_for_file: no_leading_underscores_for_library_prefixes
import 'package:auto_route/auto_route.dart' as _i4;
import 'package:flutter/material.dart' as _i5;

import '../pages/details/details_page.dart' as _i3;
import '../pages/liked_beers/liked_beers_page.dart' as _i2;
import '../pages/selector/selector_page.dart' as _i1;

class AppRouter extends _i4.RootStackRouter {
  AppRouter([_i5.GlobalKey<_i5.NavigatorState>? navigatorKey])
      : super(navigatorKey);

  @override
  final Map<String, _i4.PageFactory> pagesMap = {
    SelectorRoute.name: (routeData) {
      return _i4.CustomPage<void>(
        routeData: routeData,
        child: const _i1.SelectorPage(),
        transitionsBuilder: _i4.TransitionsBuilders.slideLeftWithFade,
        durationInMilliseconds: 300,
        opaque: true,
        barrierDismissible: false,
      );
    },
    LikedBeersRoute.name: (routeData) {
      return _i4.CustomPage<void>(
        routeData: routeData,
        child: const _i2.LikedBeersPage(),
        transitionsBuilder: _i4.TransitionsBuilders.slideLeftWithFade,
        durationInMilliseconds: 300,
        opaque: true,
        barrierDismissible: false,
      );
    },
    DetailsRoute.name: (routeData) {
      final args = routeData.argsAs<DetailsRouteArgs>();
      return _i4.CustomPage<void>(
        routeData: routeData,
        child: _i3.DetailsPage(
          key: args.key,
          index: args.index,
        ),
        transitionsBuilder: _i4.TransitionsBuilders.slideLeftWithFade,
        durationInMilliseconds: 300,
        opaque: true,
        barrierDismissible: false,
      );
    },
  };

  @override
  List<_i4.RouteConfig> get routes => [
        _i4.RouteConfig(
          '/#redirect',
          path: '/',
          redirectTo: '/selector',
          fullMatch: true,
        ),
        _i4.RouteConfig(
          SelectorRoute.name,
          path: '/selector',
        ),
        _i4.RouteConfig(
          LikedBeersRoute.name,
          path: '/liked-beers',
        ),
        _i4.RouteConfig(
          DetailsRoute.name,
          path: '/details',
        ),
      ];
}

/// generated route for
/// [_i1.SelectorPage]
class SelectorRoute extends _i4.PageRouteInfo<void> {
  const SelectorRoute()
      : super(
          SelectorRoute.name,
          path: '/selector',
        );

  static const String name = 'SelectorRoute';
}

/// generated route for
/// [_i2.LikedBeersPage]
class LikedBeersRoute extends _i4.PageRouteInfo<void> {
  const LikedBeersRoute()
      : super(
          LikedBeersRoute.name,
          path: '/liked-beers',
        );

  static const String name = 'LikedBeersRoute';
}

/// generated route for
/// [_i3.DetailsPage]
class DetailsRoute extends _i4.PageRouteInfo<DetailsRouteArgs> {
  DetailsRoute({
    _i5.Key? key,
    required int index,
  }) : super(
          DetailsRoute.name,
          path: '/details',
          args: DetailsRouteArgs(
            key: key,
            index: index,
          ),
        );

  static const String name = 'DetailsRoute';
}

class DetailsRouteArgs {
  const DetailsRouteArgs({
    this.key,
    required this.index,
  });

  final _i5.Key? key;

  final int index;

  @override
  String toString() {
    return 'DetailsRouteArgs{key: $key, index: $index}';
  }
}
