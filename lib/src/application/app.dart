import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/generated/l10n.dart';
import 'package:beer_tinder/src/services/services.dart';
import 'package:beer_tinder/src/theme/app_theme.dart';

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  State<App> createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer(builder: (context, ref, child) {
      final List<Locale> supportedLocales =
          ref.read(Services.locale.notifier).getSupportedLocales();
      final Locale activeLocale = ref.watch(Services.locale);

      final appRouter = ref.read(Services.navigator).router;

      return MaterialApp.router(
        //theme: getLightTheme(),
        routerDelegate: appRouter.delegate(),
        routeInformationParser: appRouter.defaultRouteParser(),
        locale: activeLocale,
        supportedLocales: supportedLocales,
        localizationsDelegates: const [
          S.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
      );
    },);
  }
}
