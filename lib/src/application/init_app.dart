import 'dart:async';
import 'package:flutter/material.dart';
import 'package:beer_tinder/src/store/hive/initalizer.dart';

Future<void> initApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initHiveBoxes();
}
