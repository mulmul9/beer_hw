import 'package:logger/logger.dart';

final logger = Logger(
  level: Level.debug,
  printer: PrettyPrinter(
    methodCount: 3,
    errorMethodCount: 5,
    lineLength: 50,
  ),
);
