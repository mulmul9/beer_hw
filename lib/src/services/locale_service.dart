import 'dart:io';

import 'package:flutter/widgets.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:beer_tinder/generated/l10n.dart';

class LocaleService extends StateNotifier<Locale> {
  static String boxName = 'localeBox';
  static String boxKey = 'locale';

  final Ref ref;

  // fallback locale setup
  LocaleService(this.ref) : super(const Locale('en')) {
    setDefaultLocale();
  }

  static Future<void> initBox() async {
    await Hive.openBox(boxName);
  }

  Future<void> setDefaultLocale() async {
    final Locale? locale = await restoreLocale() ?? getPlatformLocale();

    if (locale != null) {
      setActiveLocale(locale);
    }
  }

  Locale? getPlatformLocale() {
    final Locale platformLocale = Locale(Platform.localeName);

    return isLocaleSupported(platformLocale) ? platformLocale : null;
  }

  static bool isLocaleSupported(Locale locale) {
    return S.delegate.isSupported(locale);
  }

  Future<void> persistLocale(Locale locale) async {
    final box = Hive.box(boxName);

    box.put(boxKey, [locale.languageCode, locale.countryCode]);
  }

  Future<Locale?> restoreLocale() async {
    final box = Hive.box(boxName);

    final locale = box.get(boxKey);

    if (locale == null || locale.length != 2) {
      return null;
    }

    final String? languageCode = locale[0];
    final String? countryCode = locale[1];

    if (languageCode == null) {
      return null;
    }

    return Locale.fromSubtags(
      countryCode: countryCode,
      languageCode: languageCode,
    );
  }

  List<Locale> getSupportedLocales() {
    return S.delegate.supportedLocales;
  }

  Future<void> setActiveLocale(
    Locale locale,
  ) async {
    state = locale;
    persistLocale(locale);
  }
}
