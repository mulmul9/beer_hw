import 'package:flutter/material.dart';
import 'package:auto_route/auto_route.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:beer_tinder/src/application/app_router.gr.dart';
import 'package:beer_tinder/src/store/store.dart';

class NavigatorService extends ChangeNotifier {
  Ref ref;
  final AppRouter _router;

  NavigatorService(this.ref) : _router = AppRouter();

  AppRouter get router => _router;

  bool get canNavigateBack {
    switch (_router.current.name) {
      // case Route.name:
      // return false;
      default:
        return true;
    }
  }
}

// class AuthGuard extends AutoRouteGuard {
//   final Ref _ref;
//
//   AuthGuard(this._ref);
//
//   @override
//   void onNavigation(NavigationResolver resolver, StackRouter router) {
//     if (_ref.read(Store.profile).isLoggedIn) {
//       resolver.next();
//     } else {
//       router.push(const LoginRoute());
//     }
//   }
// }
//
// class LoggedInGuard extends AutoRouteGuard {
//   final Ref _ref;
//
//   LoggedInGuard(this._ref);
//
//   @override
//   void onNavigation(NavigationResolver resolver, StackRouter router) {
//     if (_ref.read(Store.profile).isLoggedIn) {
//       router.push(const DashboardRoute());
//     } else {
//       resolver.next();
//     }
//   }
// }
