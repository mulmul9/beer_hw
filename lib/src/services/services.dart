import 'package:flutter/material.dart';
import 'package:beer_tinder/src/services/locale_service.dart';
import 'package:beer_tinder/src/services/navigator_service.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';

import 'api_service.dart';

class Services {
  static final ChangeNotifierProvider<ApiService> api =
      ChangeNotifierProvider<ApiService>((ref) => ApiService(ref));

  static final locale = StateNotifierProvider<LocaleService, Locale>(
    (ref) => LocaleService(ref),
    name: 'LocaleService',
  );

  static final ChangeNotifierProvider<NavigatorService> navigator =
      ChangeNotifierProvider<NavigatorService>(NavigatorService.new);

}
