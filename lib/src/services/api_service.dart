import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_riverpod/flutter_riverpod.dart';
import 'package:http/http.dart' as http;

import 'package:beer_tinder/src/widgets/beer_data.dart';

class ApiService extends ChangeNotifier {
  final Ref ref;
  final String apiUrl = 'https://api.punkapi.com/v2/beers';

  Future<List<dynamic>> fetchBeers() async {
    final response = await http.get(Uri.parse(apiUrl));

    if (response.statusCode == 200) {
      return json.decode(response.body);
    } else {
      throw Exception('Failed to load beers');
    }
  }

  Future<BeerData> fetchRandomBeer() async {
    final response = await http.get(Uri.parse('$apiUrl/random'));
    if (response.statusCode == 200) {
      final responseData = json.decode(response.body);

      final Map<String, dynamic> beerJson = responseData[0];

      return BeerData(
        name: beerJson['name'] ?? '',
        id: beerJson['id'] ?? -1,
        tagline: beerJson['tagline'] ?? '',
        imageUrl: beerJson['image_url'] ?? '',
        description: beerJson['description'],
      );
    } else {
      throw Exception('Failed to load beers');
    }
  }

  ApiService(this.ref);
}