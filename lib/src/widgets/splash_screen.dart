import 'package:flutter/material.dart';

import 'package:beer_tinder/src/theme/styles.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Stack(
        children: [
          Positioned.fill(
            child: Container(
              decoration: BoxDecoration(
                gradient: LinearGradient(
                  colors: [
                    $styles.colors.linearGradientBackgroundPrimaryFromColor,
                    $styles.colors.linearGradientBackgroundPrimaryToColor
                  ],
                  begin: Alignment.bottomLeft,
                  end: Alignment.topRight,
                ),
              ),
            ),
          ),
          Center(
            child: Image.asset('assets/images/logo/logo_full.png'),
          ),
        ],
      ),
    );
  }
}
