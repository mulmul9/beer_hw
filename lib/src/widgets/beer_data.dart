class BeerData {
  final String name;
  final int id;
  final String tagline;
  final String? description;
  final String imageUrl;

  const BeerData({
    required this.name,
    required this.id,
    required this.tagline,
    required this.imageUrl,
    this.description,
  });
}