import 'package:flutter/material.dart';
import 'package:beer_tinder/src/theme/styles.dart';
import 'package:beer_tinder/src/util/constants.dart';
import 'package:beer_tinder/src/widgets/app_scroll_behavior.dart';

class MainLayout extends StatelessWidget {
  final Widget children;
  final PreferredSizeWidget? appBar;

  static AppStyle get style => _style;
  static final AppStyle _style = AppStyle();

  const MainLayout({
    super.key,
    required this.children,
    this.appBar,
  });

  @override
  Widget build(BuildContext context) {
    var queryData = MediaQuery.of(context);

    return Scaffold(
      appBar: queryData.size.width < mobileWidth ? appBar : null,
      body: Theme(
        data: $styles.colors.toThemeData(),
        // Provide a default texts style to allow Hero's to render text properly
        child: DefaultTextStyle(
          style: $styles.text.body,
          // Use a custom scroll behavior across entire app
          child: ScrollConfiguration(
            behavior: AppScrollBehavior(),
            child: children,
          ),
        ),
      ),
    );
  }
}
