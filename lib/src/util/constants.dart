const double kVerticalSpaceHalf = 8.0;
const double kVerticalSpaceDefault = 16.0;
const double kVerticalSpaceDouble = 32.0;
const double kVerticalSpaceTriple = 48.0;
const double kVerticalSpaceLarge = 64.0;

const double kHorizontalSpaceHalf = 8.0;
const double kHorizontalSpaceDefault = 16.0;
const double kHorizontalSpaceDouble = 32.0;
const double kHorizontalSpaceTriple = 48.0;
const double kHorizontalSpaceLarge = 64.0;

const mobileWidth = 1100.0;
