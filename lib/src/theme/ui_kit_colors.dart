import 'package:flutter/material.dart';

class UiKitColors {
  //UiKitColors._();
  final Color primary = const Color(0xff023047);
  final Color secondary = const Color(0xffF4EBD0);
  final Color error = const Color(0xFFF44336);

  final Color accent1 = const Color(0xFFE4935D);
  final Color black = const Color(0xFF1E1B18);
  final Color white = Colors.white;
  final Color offWhite = const Color(0xFFF8ECE5);
  final Color greyStrong = const Color(0xFF272625);
  final Color greyMedium = const Color(0xFF9D9995);

  final Color primaryText = const Color(0xff2D2926);
  final Color secondaryText = const Color(0xff817F7D);
  final Color disabledText = const Color(0xffC0BFBE);

  final Color primaryBackground = const Color(0xffF8F6F4);
  final Color secondaryBackground = const Color(0xffF5F4F4);
  final Color buttonPrimaryBackground = const Color(0xff023047);
  final Color buttonSecondaryBackground = const Color(0xff6631F1);
  final Color transparent = const Color(0x00FFFFFF);
  final Color cardOverlay = const Color(0xAA000000);
  final Color progressBarBackgroundColor = Colors.grey.shade300;
  final Color snackbarBackgroundColor = Colors.black;
  final Color linearGradientBackgroundPrimaryFromColor = const Color(0xff023047);
  final Color linearGradientBackgroundPrimaryToColor = const Color(0xff023047);
  final List<Color> liquidSwipeBackgroundColors = [
    const Color(0xffB68D40),
    const Color(0xff870A30),
    const Color(0xffD6AD60),
    const Color(0xff603F8B),
    const Color(0xff3AA3A0)
  ];
  final Color googleSignInButtonPrimaryColor = Colors.white;
  final Color googleSignInButtonOnPrimaryColor = Colors.grey;
  final Color facebookSignInButtonColor = const Color(0xff1877F2);

  final bool isDark = false;

  ThemeData toThemeData() {
    /// Create a TextTheme and ColorScheme, that we can use to generate ThemeData
    TextTheme txtTheme = (isDark ? ThemeData.dark() : ThemeData.light()).textTheme;
    Color txtColor = Colors.white;
    ColorScheme colorScheme = ColorScheme(
      // Map our custom theme to the Material ColorScheme
        brightness: isDark ? Brightness.dark : Brightness.light,
        primary: primary,
        primaryContainer: primary,
        secondary: secondary,
        secondaryContainer: secondary,
        background: primaryBackground,
        surface: primaryBackground,
        onBackground: txtColor,
        onSurface: txtColor,
        onError: Colors.white,
        onPrimary: Colors.white,
        onSecondary: Colors.white,
        error: error,);

    /// Now that we have ColorScheme and TextTheme, we can create the ThemeData
    /// Also add on some extra properties that ColorScheme seems to miss
    var t = ThemeData.from(textTheme: txtTheme, colorScheme: colorScheme).copyWith(
      textSelectionTheme: TextSelectionThemeData(cursorColor: primary),
      highlightColor: primary,
    );

    /// Return the themeData which MaterialApp can now use
    return t;
  }
}
