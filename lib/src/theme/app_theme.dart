class UiKitConstants {
  UiKitConstants._();
  static const double kVerticalSpace4 = 4.0;
  static const double kVerticalSpace8 = 8.0;
  static const double kVerticalSpace16 = 16.0;
  static const double kVerticalSpace24 = 24.0;
  static const double kVerticalSpace32 = 32.0;
  static const double kVerticalSpace40 = 40.0;
  static const double kVerticalSpace48 = 48.0;
  static const double kVerticalSpace64 = 64.0;
  static const double kVerticalSpace72 = 72.0;
  static const double kVerticalSpace80 = 80.0;

  static const double kHorizontalSpace8 = 8.0;
  static const double kHorizontalSpace16 = 16.0;
  static const double kHorizontalSpace24 = 24.0;
  static const double kHorizontalSpace32 = 32.0;
  static const double kHorizontalSpace40 = 40.0;
  static const double kHorizontalSpace48 = 48.0;
  static const double kHorizontalSpace64 = 64.0;
  static const double kHorizontalSpace72 = 72.0;
  static const double kHorizontalSpace80 = 80.0;

  static const double kIconSize16 = 16.0;
  static const double kIconSize24 = 24.0;
  static const double kIconSize32 = 32.0;

  static const double kButtonHeight55 = 55.0;

  static const int quarterTurns0 = 0;
  static const int quarterTurns90 = 90;
  static const int quarterTurns180 = 180;
}
